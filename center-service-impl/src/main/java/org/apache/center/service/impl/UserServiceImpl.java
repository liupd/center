package org.apache.center.service.impl;

import org.springframework.stereotype.Component;
import com.alibaba.dubbo.config.annotation.Service;

import org.apache.playframework.service.impl.BaseServiceImpl;
import org.apache.center.service.UserService;
import org.apache.center.model.User;
import org.apache.center.dao.UserMapper;
/**
 *
 * User 表数据服务层接口实现类
 *
 */
@Component  
@Service
public class UserServiceImpl extends BaseServiceImpl<UserMapper, User> implements UserService {


}


